//
//  BusyLight.h
//  LightControlMenu
//
//  Created by Eric Betts on 11/21/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IOKit/hid/IOHIDLib.h>
#define TIMER_REPEAT 9


@interface BusyLight : NSObject {
    
}

@property IOHIDDeviceRef busylight;
-(void) initUsb;

+(BusyLight*) getInstance;
@property NSString *currentColor;
@property NSTimer* timer;

@end
