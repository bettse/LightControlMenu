//
//  main.m
//  LightControlMenu
//
//  Created by Eric Betts on 11/15/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
