//
//  UsbMailNotifier.m
//  LightControlMenu
//
//  Created by Eric Betts on 11/15/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import "UsbMailNotifier.h"

@implementation UsbMailNotifier

static UsbMailNotifier* singleton;

+(UsbMailNotifier*) getInstance
{
    if (!singleton){
        singleton = [[self alloc] init];
    }
    return singleton;
}

- (NSData*) commandForColor:(NSString*)color
{
    uint8_t bytes[5] = {0};
    if ([color isEqualToString:@"red"]) {
        bytes[0] = 2;
    } else if ([color isEqualToString:@"green"]) {
        bytes[0] = 3;
    } else if ([color isEqualToString:@"blue"]) {
        bytes[0] = 1;
    } else if ([color isEqualToString:@"white"]) {
        bytes[0] = 7;
    } else if ([color isEqualToString:@"off"]) {
        bytes[0] = 0;
    }
    
    return [NSData dataWithBytes:bytes length:sizeof(bytes)];
}


- (void) setColor:(NSNotification *) notification
{
    NSDictionary *userInfo = notification.userInfo;
    NSString *newColor = [userInfo[@"newColor"] lowercaseString];
    NSData *command = [self commandForColor:newColor];
    long reportSize = 0;
    if (self.umn) {
        NSLog(@"%s %@", __PRETTY_FUNCTION__, command);
        IOHIDDeviceSetReport(self.umn, kIOHIDReportTypeOutput, reportSize, (uint8_t *)[command bytes], [command length]);
    }
}

void MyInputCallback(void *context, IOReturn result, void *sender, IOHIDReportType type, uint32_t reportID, uint8_t *report, CFIndex reportLength)
{
    //NSLog(@"MyInputCallback called");
    //process device response buffer (report) here
}

//http://developer.apple.com/library/mac/#documentation/DeviceDrivers/Conceptual/HID/new_api_10_5/tn2187.html
// function to get a long device property
// returns FALSE if the property isn't found or can't be converted to a long
static Boolean IOHIDDevice_GetLongProperty(IOHIDDeviceRef inDeviceRef, CFStringRef inKey, long * outValue)
{
    Boolean result = FALSE;
    CFTypeRef tCFTypeRef = IOHIDDeviceGetProperty(inDeviceRef, inKey);
    if (tCFTypeRef) {
        // if this is a number
        if (CFNumberGetTypeID() == CFGetTypeID(tCFTypeRef)) {
            // get its value
            result = CFNumberGetValue((CFNumberRef) tCFTypeRef, kCFNumberSInt32Type, outValue);
        }
    }
    return result;
}


// this will be called when the HID Manager matches a new (hot plugged) HID device
static void Handle_DeviceMatchingCallback(void* inContext, IOReturn inResult, void* inSender, IOHIDDeviceRef inIOHIDDeviceRef)
{
    @autoreleasepool {
        long reportSize = 0;
        UsbMailNotifier* self = (__bridge UsbMailNotifier*) inContext;
        self.umn = inIOHIDDeviceRef;
        uint8_t *report;
        (void)IOHIDDevice_GetLongProperty(inIOHIDDeviceRef, CFSTR(kIOHIDMaxInputReportSizeKey), &reportSize);
        if (reportSize) {
            report = calloc(1, reportSize);
            if (report) {
                IOHIDDeviceRegisterInputReportCallback(inIOHIDDeviceRef, report, reportSize, MyInputCallback, inContext);
                [[NSNotificationCenter defaultCenter] postNotificationName: @"deviceConnected" object:nil userInfo:@{@"class": NSStringFromClass([self class])}];
            }
        }
    }
}

// this will be called when a HID device is removed (unplugged)
static void Handle_DeviceRemovalCallback(void* inContext, IOReturn inResult, void* inSender, IOHIDDeviceRef inIOHIDDeviceRef)
{
    UsbMailNotifier* self = (__bridge UsbMailNotifier*) inContext;
    [[NSNotificationCenter defaultCenter] postNotificationName: @"deviceDisconnected" object:nil userInfo:@{@"class": NSStringFromClass([self class])}];
}

-(void) initUsb
{
    @autoreleasepool {
        const long productId = 0x1320;
        const long vendorId = 0x1294;
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        IOHIDManagerRef managerRef = IOHIDManagerCreate(kCFAllocatorDefault, kIOHIDOptionsTypeNone);
        IOHIDManagerScheduleWithRunLoop(managerRef, CFRunLoopGetMain(), kCFRunLoopDefaultMode);
        IOHIDManagerOpen(managerRef, 0L);
        dict[@kIOHIDProductIDKey] = @(productId);
        dict[@kIOHIDVendorIDKey] = @(vendorId);
        IOHIDManagerSetDeviceMatching(managerRef, (__bridge CFMutableDictionaryRef)dict);
        IOHIDManagerRegisterDeviceMatchingCallback(managerRef, Handle_DeviceMatchingCallback, (__bridge void *)(self));
        IOHIDManagerRegisterDeviceRemovalCallback(managerRef, Handle_DeviceRemovalCallback, (__bridge void *)(self));
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setColor:) name:@"setColor" object:nil];

        //NSLog(@"Starting runloop");
        [[NSRunLoop currentRunLoop] run];
    }
}

@end
