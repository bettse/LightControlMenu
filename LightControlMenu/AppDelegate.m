//
//  AppDelegate.m
//  LightControlMenu
//
//  Created by Eric Betts on 11/15/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
    [NSApp setActivationPolicy:NSApplicationActivationPolicyProhibited]; //hide dock icon
    [self addMenubarIcon];
    
    self.stateToColor = @{
                          @"Off": @"Off",
                          @"Available": @"green",
                          @"Busy": @"red",
                          @"On": @"white"
                          };
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceConnected:) name:@"deviceConnected" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceDisconnected:) name:@"deviceDisconnected" object:nil];
    
    //Spin off daemon into its own thread
    self.umn = [UsbMailNotifier getInstance];
    self.daemon = [[NSThread alloc] initWithTarget:self.umn selector:@selector(initUsb) object:nil];
    [self.daemon start];
    
    //Spin off busylight daemon into its own thread
    self.busylight = [BusyLight getInstance];
    self.busyLightDaemon = [[NSThread alloc] initWithTarget:self.busylight selector:@selector(initUsb) object:nil];
    [self.busyLightDaemon start];
}
- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName: @"setColor" object:nil userInfo:@{@"newColor": @"off"}];
    [self.daemon cancel];
    return NSTerminateNow;
}

- (void)addMenubarIcon
{
    NSImage* icon = [NSImage imageNamed:@"clear"];
    icon.size = NSMakeSize(18.0, 18.0);
    self.currentState = @"Off";
    
    if (!self.statusItem) {
        self.statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength];
        self.statusItem.highlightMode = YES;
        //self.statusItem.title = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
        //self.statusItem.menu = self.statusMenu;
        self.statusItem.action = @selector(menuClick:);
        self.statusItem.target = self;
    }
    self.statusItem.image = icon;
}

-(void)menuClick:(id)sender
{
    
    NSEvent *event = [NSApp currentEvent];
    if([event modifierFlags] & NSControlKeyMask) {
        //Ctrl click
        [self.statusItem popUpStatusItemMenu:self.statusMenu];
    } else {
        //Left click
        [self menuClickLeft:sender];
    }
}

-(void)menuClickLeft:(id)sender {
    NSImage* icon;
    if([self.currentState isEqualToString:@"Available"]) {
        self.currentState = @"Busy";
        icon = [NSImage imageNamed:@"red"];
    } else if([self.currentState isEqualToString:@"Busy"]) {
        self.currentState = @"Off";
        icon = [NSImage imageNamed:@"clear"];
    } else if([self.currentState isEqualToString:@"Off"]) {
        self.currentState = @"On";
        icon = [NSImage imageNamed:@"clear"];
    } else if([self.currentState isEqualToString:@"On"]) {
        self.currentState = @"Available";
        icon = [NSImage imageNamed:@"green"];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"setColor" object:nil userInfo:@{@"newColor": self.stateToColor[self.currentState]}];
    icon.size = NSMakeSize(18.0, 18.0);
    self.statusItem.image = icon;
}

-(void) deviceConnected:(NSNotification *) notification
{
    NSDictionary *userInfo = notification.userInfo;
    NSLog(@"Device connected [%@]: sending color %@", userInfo[@"class"], self.stateToColor[self.currentState]);
    [[NSNotificationCenter defaultCenter] postNotificationName: @"setColor" object:nil userInfo:@{@"newColor": self.stateToColor[self.currentState]}];
}

-(void) deviceDisconnected:(NSNotification *) notification
{
    NSDictionary *userInfo = notification.userInfo;
    NSLog(@"Device disconnected [%@]", userInfo[@"class"]);
}

@end
