//
//  UsbMailNotifier.h
//  LightControlMenu
//
//  Created by Eric Betts on 11/15/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IOKit/hid/IOHIDLib.h>
#include <stdlib.h>

#define OFF 0x00
#define BLUE 0x01
#define RED 0x02
#define GREEN 0x03
#define LTBLUE 0x04
#define PURPLE 0x05
#define YELLOW 0x06
#define WHITE 0x07

@interface UsbMailNotifier : NSObject {
    
}

@property IOHIDDeviceRef umn;
-(void) initUsb;
-(void) setColor:(NSNumber*)n;

+(UsbMailNotifier*) getInstance;

@end