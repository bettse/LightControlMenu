//
//  BusyLight.m
//  LightControlMenu
//
//  Created by Eric Betts on 11/21/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import "BusyLight.h"

@implementation BusyLight

static BusyLight* singleton;

+(BusyLight*) getInstance
{
    if (!singleton){
        singleton = [[self alloc] init];
        singleton.currentColor = @"off";
    }
    return singleton;
}

- (NSData*) commandForColor:(NSString*)color
{
    uint8_t bytes[5] = {0};
    if ([color isEqualToString:@"red"]) {
        bytes[2] = 0xFF;
    } else if ([color isEqualToString:@"green"]) {
        bytes[3] = 0xFF;
    } else if ([color isEqualToString:@"blue"]) {
        bytes[4] = 0xFF;
    } else if ([color isEqualToString:@"white"]) {
        bytes[2] = 0xFF;
        bytes[3] = 0xFF;
        bytes[4] = 0xFF;
    } else if ([color isEqualToString:@"off"]) {
        bytes[2] = 0;
        bytes[3] = 0;
        bytes[4] = 0;
    } else {
        //#color given
    }

    return [NSData dataWithBytes:bytes length:sizeof(bytes)];
}

- (void) acceptNotification:(NSNotification *) notification
{
    NSDictionary *userInfo = notification.userInfo;
    self.currentColor = [userInfo[@"newColor"] lowercaseString];
    //NSLog(@"%s %@", __PRETTY_FUNCTION__, self.currentColor);
    [self updateColor];
}

- (void) updateColor
{
    NSData *command = [self commandForColor:self.currentColor];
    long reportSize = 0;
    if (self.busylight) {
        //NSLog(@"%s %@", __PRETTY_FUNCTION__, command);
        IOHIDDeviceSetReport(self.busylight, kIOHIDReportTypeOutput, reportSize, (uint8_t *)[command bytes], [command length]);
    }
}

void BusyLightCallback(void *inContext, IOReturn result, void *sender, IOHIDReportType type, uint32_t reportID, uint8_t *report, CFIndex reportLength)
{
    //BusyLight* self = (__bridge BusyLight*) inContext;
    if(reportLength == 0 || report[1] == 0) {
        return;
    }else if(reportLength == 64){
        //NSLog(@"%s: %@", __FUNCTION__, [NSData dataWithBytes:report length:reportLength]);
    }
    
}

//http://developer.apple.com/library/mac/#documentation/DeviceDrivers/Conceptual/HID/new_api_10_5/tn2187.html
// function to get a long device property
// returns FALSE if the property isn't found or can't be converted to a long
static Boolean IOHIDDevice_GetLongProperty(IOHIDDeviceRef inDeviceRef, CFStringRef inKey, long * outValue)
{
    Boolean result = FALSE;
    CFTypeRef tCFTypeRef = IOHIDDeviceGetProperty(inDeviceRef, inKey);
    if (tCFTypeRef) {
        // if this is a number
        if (CFNumberGetTypeID() == CFGetTypeID(tCFTypeRef)) {
            // get its value
            result = CFNumberGetValue((CFNumberRef) tCFTypeRef, kCFNumberSInt32Type, outValue);
        }
    }
    return result;
}

// this will be called when the HID Manager matches a new (hot plugged) HID device
static void Handle_DeviceMatchingCallback(void* inContext, IOReturn inResult, void* inSender, IOHIDDeviceRef inIOHIDDeviceRef)
{
    @autoreleasepool {
        long reportSize = 0;
        BusyLight* self = (__bridge BusyLight*) inContext;
        self.busylight = inIOHIDDeviceRef;
        uint8_t *report;
        (void)IOHIDDevice_GetLongProperty(inIOHIDDeviceRef, CFSTR(kIOHIDMaxInputReportSizeKey), &reportSize);
        if (reportSize) {
            report = calloc(1, reportSize);
            if (report) {
                IOHIDDeviceRegisterInputReportCallback(inIOHIDDeviceRef, report, reportSize, BusyLightCallback, inContext);
                [[NSNotificationCenter defaultCenter] postNotificationName: @"deviceConnected" object:nil userInfo:@{@"class": NSStringFromClass([self class])}];
                //Setup timer to reassert color since Kuando Busylight automatically shuts off
                self.timer = [NSTimer scheduledTimerWithTimeInterval:TIMER_REPEAT target:self selector:@selector(updateColor) userInfo:nil repeats:YES];
            }
        }
    }
}

// this will be called when a HID device is removed (unplugged)
static void Handle_DeviceRemovalCallback(void* inContext, IOReturn inResult, void* inSender, IOHIDDeviceRef inIOHIDDeviceRef)
{
    @autoreleasepool {
        BusyLight* self = (__bridge BusyLight*) inContext;
        self.busylight = nil;
        [self.timer invalidate];
        [[NSNotificationCenter defaultCenter] postNotificationName: @"deviceDisconnected" object:nil userInfo:@{@"class": NSStringFromClass([self class])}];
    }
}

-(void) initUsb
{
    @autoreleasepool {
        const long vendorId = 0x04D8;
        const long productId = 0xF848;
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        IOHIDManagerRef managerRef = IOHIDManagerCreate(kCFAllocatorDefault, kIOHIDOptionsTypeNone);
        IOHIDManagerScheduleWithRunLoop(managerRef, CFRunLoopGetMain(), kCFRunLoopDefaultMode);
        IOHIDManagerOpen(managerRef, 0L);
        dict[@kIOHIDProductIDKey] = @(productId);
        dict[@kIOHIDVendorIDKey] = @(vendorId);
        IOHIDManagerSetDeviceMatching(managerRef, (__bridge CFMutableDictionaryRef)dict);
        IOHIDManagerRegisterDeviceMatchingCallback(managerRef, Handle_DeviceMatchingCallback, (__bridge void *)(self));
        IOHIDManagerRegisterDeviceRemovalCallback(managerRef, Handle_DeviceRemovalCallback, (__bridge void *)(self));
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(acceptNotification:) name:@"setColor" object:nil];
        //NSLog(@"Starting runloop");
        [[NSRunLoop currentRunLoop] run];
    }
}

@end
