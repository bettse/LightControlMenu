//
//  AppDelegate.h
//  LightControlMenu
//
//  Created by Eric Betts on 11/15/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "UsbMailNotifier.h"
#import "BusyLight.h"

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

//Status Bar
@property IBOutlet NSMenu *statusMenu;
@property NSStatusItem *statusItem;
@property IBOutlet NSMenuItem *startAtLogin;
@property IBOutlet NSMenuItem *useBWIcon;

@property NSThread* daemon;
@property UsbMailNotifier *umn;

@property NSThread* busyLightDaemon;
@property BusyLight *busylight;

@property NSString* currentState;

@property NSDictionary *stateToColor;
@end
